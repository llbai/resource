#code=utf-8
# 生成定制的图片和视频（定制的图片格式，制式及大小）（定制的视频格式，帧率及大小）
# 图片mode共9种，分别为1，L，P，RGB，RGBA，CMYK，YCbCr，I，F
# 视频

from PIL import Image
import cv2
import numpy as np
import random
import imageio
from itertools import product
img_modes = ["1","L","RGB"]
img_mods = ["jpg","jpeg","bmp","png"]

video_mod = {
    "mp4":'MP4V',
    # "avi":'XVID',
    # "mov":'MP4V',
    # "flv":'FLV1'
}
    
    

# 生成指定宽高的图片 width,height demo.bmp
# 传入参数：图片宽int，高int，彩色模式str，图片名str
# 传出参数：无
def new_img(width,height,mode,name):
    if mode == "1" or mode == "L":
        color = None
    else:
        color = (random.randint(0,255),random.randint(0,255),random.randint(0,255))
    img = Image.new(mode = mode, size = (width,height), color = color)
    img.save(name)

# 生成指定文件大小的图片 kb demo.bmp
# 传入参数：图片占用存储大小 int kb
# 传出参数：无
def new_img_size(size):
    img = Image.new(mode = "L", size = (size,size), color = '#66ccff')
    img.save("demo.bmp")

# 生成指定时间长度gif
# 传入参数：gif尺寸长，宽，图片间隔，图片数量，图片名
# 传出参数：无
def new_gif(width,height,mode,duration,img_num,name):
    imgs = []
    if mode == "1" or mode == "L":
        color = None
        for i in range(img_num):
            imgs.append(Image.new(mode = mode, size = (width,height), color = color))
    else:
        for i in range(img_num):
            color = random.randint(0,255),random.randint(0,255),random.randint(0,255)
            imgs.append(Image.new(mode = mode, size = (width,height), color = color))
    imgs[0].save(name, save_all=True, append_images=imgs, duration=duration)


# 生成视频（RGB纯色图无音乐）result.mp4
# 传入参数：视频长度int，宽int，时间int秒，帧数int，格式str
# 传出参数：无
def new_video_rgb(width,height,time,fps,mod,name):
    size = (width, height)
    # fourcc = cv2.VideoWriter_fourcc(*'XVID')
    mod = video_mod[mod]
    fourcc = cv2.VideoWriter_fourcc(*mod)
    videowriter = cv2.VideoWriter(name,fourcc, 100, size)

    img_red = np.zeros([width,height, 3], np.uint8)
    img_red[:, :, 2] = np.zeros([width,height])+255

    img_green = np.zeros([width,height, 3], np.uint8)
    img_green[:, :, 0] = np.zeros([width,height])+255

    img_blue = np.zeros([width,height, 3], np.uint8)
    img_blue[:, :, 1] = np.zeros([width,height])+255

    for i in range(62):
        img = random.choice([img_blue,img_red,img_green])
        for j in range(1):
            #生成图片视频
            # img = cv2.imread('demo.bmp')  
            # img = cv2.imread(img)
            img=cv2.resize(img,(width,height))
            videowriter.write(img)

def main():
    # 测试图片生成
    img_task_list = []
    # 生成1x1 图片包
    width,height = 1,1
    img_task_list.append([width,height])
    # 1080x1920 图片包
    width,height = 1080,1920
    img_task_list.append([width,height])
    # 1x12032 图片包
    width,height = 1,12032
    img_task_list.append([width,height])
    # 12032x1 图片包
    width,height = 12032,1
    img_task_list.append([width,height])
    # 1亿像素（9024x12032）图片包
    width,height = 9024,12032
    img_task_list.append([width,height])
    # 完成图片生成
    for img_task,mod,mode in product(img_task_list,img_mods,img_modes):
        width,height = img_task
        name = str(width)+"x"+str(height)+"_"+mode+"."+mod
        new_img(width,height,mode,name)


    # 测试动图生成
    gif_task_list = list()
    # 生成最小大小gif图（1像素1图像0帧）
    width,height,duration,img_num = 1,1,0,1
    gif_task_list.append([width,height,duration,img_num])
    # 生成正常大小gif图（200像素，）
    width,height,duration,img_num = 200,200,300,10
    gif_task_list.append([width,height,duration,img_num])
    # 生成超长动图
    width,height,duration,img_num = 200,200,300,1000
    gif_task_list.append([width,height,duration,img_num])
    # 生成超长宽动图
    width,height,duration,img_num = 9024,12032,300,100
    gif_task_list.append([width,height,duration,img_num])
    # 生成超长宽超大动图
    width,height,duration,img_num = 9024,12032,300,1000
    # gif_task_list.append([width,height,duration,img_num])
    # 完成动图生成
    for gif_task,mode in product(gif_task_list,img_modes):
        width,height,duration,img_num = gif_task
        name = str(width)+"x"+str(height)+"_"+mode+"_"+str(duration*img_num)+".gif"
        new_gif(width,height,mode,duration,img_num,name)
    print("gif task down")


    # 测试视频生成
    video_task_list = list()
    # 生成1帧1像素超小视频包
    width,height,time,fps,name = 2,2,1,1,"4p1hz1s."
    video_task_list.append([width,height,time,fps,name])
    # 生成1帧720p视频包
    width,height,time,fps,name = 720,480,1,1,"720p1hz1s."
    video_task_list.append([width,height,time,fps,name])
    # 生成10分钟720p视频包
    width,height,time,fps,name = 720,480,600,30,"720p30hz10m."
    # video_task_list.append([width,height,time,fps,name]) # 暂时注释掉，生成有点慢，后面用多进程试试吧
    # 生成14分钟4k60hz 视频包
    width,height,time,fps,name = 4096,2304,900,60,"4k60hz15m."
    # video_task_list.append([width,height,time,fps,name])  # 暂时注释掉，生成太慢，后面用多进程试试吧
    for mod,video_task in product(video_mod.keys(),video_task_list):
        width,height,time,fps,name = video_task
        name = name + mod
        new_video_rgb(width,height,time,fps,mod,name)

    
if '__main__' == __name__:
    # main()
    video_task_list = list()
    width,height,time,fps,name = 720,480,899,30,"720p100hz620ms."
    video_task_list.append([width,height,time,fps,name])
    for mod,video_task in product(video_mod.keys(),video_task_list):
        width,height,time,fps,name = video_task
        name = name + mod
        new_video_rgb(width,height,time,fps,mod,name)